# Copyright (C) 2024 MediaTek, Inc.
# Author: Ramax Lo <ramax.lo@mediatek.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require rity-demo-image.bb

DESCRIPTION = "Rity Browser Image"

IMAGE_INSTALL += " \
	chromium-ozone-wayland \
	source-han-sans-cn-fonts \
	source-han-sans-jp-fonts \
	source-han-sans-kr-fonts \
	source-han-sans-tw-fonts \
"

