FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://0001-backend-drm-Always-mark-linear-modifier-as-supported.patch \
"

PACKAGECONFIG:append = " launcher-libseat"

EXTRA_OEMESON += " -Ddeprecated-wl-shell=true"
